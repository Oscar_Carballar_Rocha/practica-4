@file:Suppress("DEPRECATION")

package mx.unitec.practica4

import android.app.DownloadManager
import android.content.Context
import android.preference.PreferenceManager

private const val PREF_RFC ="dataRFC"

object DataPreferences {

    fun getStoredRFC(context: Context) : String{

        val prefs = PreferenceManager.getDefaultSharedPreference(context)

        return prefs.getString(PREF_RFC, delValue"") !!

    }

    fun setStoredRFC(context: Context, query: String ) {
         PreferenceManager.getDefaultSharedPreferences(context)
            .edit()
             .putString(PREF_RFC,query)
             .apply()


    }
}




